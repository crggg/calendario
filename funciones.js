

var dias = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
var meses = {
  1 : ['Enero', 31],
  2 : ['Febrero', 28],
  3 : ['Marzo', 31],
  4 : ['Abril', 30],
  5 : ['Mayo', 31],
  6 : ['Junio', 30],
  7 : ['Julio', 31],
  8 : ['Agosto', 31],
  9 : ['Septiembre', 30],
  10 : ['Octubre', 31],
  11 : ['Noviembre', 30],
  12 : ['Diciembre', 31]
};
var hoy = new Date();
var anoDefault = hoy.getFullYear();
var mesDefault = saberMesporNumero(hoy.getMonth()+1);
var diaDefault = hoy.getDate();
var diaBuscador = 9999;

window.onload = function () {

  var load = function () {
    var calendarioo = document.getElementById('dias');
    [].forEach.call(dias, function (i) {
      calendarioo.innerHTML += '<p class="dias">'+i+'</p>';
    });
  };


  load();
  calendario();
  document.getElementById('<').onclick = cambiarAno;
  document.getElementById('>').onclick = cambiarAno;
  document.getElementById('<mes').onclick = cambiarMes;
  document.getElementById('>mes').onclick = cambiarMes;

};


function cambiarAno() {

  console.log('ID: ' + this.id);
  switch (this.id)
  {
    case '<':
    {
      anoDefault -= 1;
      break;
    }
    case '>':
      anoDefault += 1;
      break;
  }
  resetearActivos();
  calendario();
}

function cambiarMes() {
  var nMes = saberMesporNombre(mesDefault);
  switch (this.id)
  {
    case '<mes':
    {
      if (nMes == 1) {
        mesDefault = meses[12][0];
        anoDefault -= 1;
      } else {
        mesDefault = meses[nMes - 1][0];
      }

      break;
    }
    case '>mes':
      if (nMes == 12) {
        mesDefault = meses[1][0];
        anoDefault += 1;
      } else {
        mesDefault = meses[nMes + 1][0];
      }
      break;
  }
  resetearActivos();
  calendario();
}

function saberMesporNombre () {
  if (mesDefault === 'Enero')
    return 1;
  else if (mesDefault === 'Febrero')
    return 2;
  else if (mesDefault === 'Marzo')
    return 3;
  else if (mesDefault === 'Abril')
    return 4;
  else if (mesDefault === 'Mayo')
    return 5;
  else if (mesDefault === 'Junio')
    return 6;
  else if (mesDefault === 'Julio')
    return 7;
  else if (mesDefault === 'Agosto')
    return 8;
  else if (mesDefault === 'Septiembre')
    return 9;
  else if (mesDefault === 'Octubre')
    return 10;
  else if (mesDefault === 'Noviembre')
    return 11;
  else if (mesDefault === 'Diciembre')
    return 12;
}

function saberMesporNumero(mes) {
  for (i in meses) {
    if (i == mes) {
      return meses[i][0];
    }
  }
}


function saberDia (Dia) {

  if (Dia === 1)
    return 'Lunes';
  else if (Dia === 2)
    return 'Martes';
  else if (Dia === 3)
    return 'Miercoles';
  else if (Dia === 4)
    return 'Jueves';
  else if (Dia === 5)
    return 'Viernes';
  else if (Dia === 6)
    return 'Sabado';
  else if (Dia === 7)
    return 'Domingo';
}

function posicion( x_nMonth, x_nDay, x_nYear) {

  if(x_nMonth >= 3){
    x_nMonth -= 2;
  }
  else {
    x_nMonth += 10;
  }

  if( (x_nMonth == 11) || (x_nMonth == 12) ){
    x_nYear--;
  }

  var nCentNum = parseInt(x_nYear / 100);
  var nDYearNum = x_nYear % 100;

  var g = parseInt(2.6 * x_nMonth - .2);

  g +=  parseInt(x_nDay + nDYearNum);
  g += nDYearNum / 4;
  g = parseInt(g);
  g += parseInt(nCentNum / 4);
  g -= parseInt(2 * nCentNum);
  g %= 7;

  if(x_nYear >= 1700 && x_nYear <= 1751) {
    g -= 3;
  }
  else {
    if(x_nYear <= 1699) {
      g -= 4;
    }
  }

  if(g < 0){
    g += 7;
  }

  return g;
}

function numeroDias(meses, mes) {
  for (i in meses) {
    if (meses[i][0] == mes) {
      return meses[i][1];
    }
  }
}

function calculaDiasMesPasado (actual, _posicionActual) {
  var mes = actual;
  if(actual == 1){
    mes = 12;
  }else {
    mes -= 1;
  }
  var mesAnterior = parseInt(meses[mes][1]);
  return (mesAnterior - (_posicionActual - 2));
}

function resetearActivos () {
  diaBuscador = 9999;
  var actives = document.getElementsByClassName('pressed');
  [].forEach.call(actives, function (i) {
    i.className = 'dias_n siActual';
  })
}

function muestraFecha(dia) {
  var array = dia.className.split(' ');
  if (array.indexOf('siActual') != -1) {
    resetearActivos();
    dia.className += ' pressed';
    document.getElementById('data').innerHTML = 'Fecha seleccionada: ' + dia.id + '/'+ saberMesporNombre(mesDefault) + '/' + anoDefault;
  }
}

function buscarFecha() {
  var value = document.getElementById('buscador').elements.namedItem('fecha');
  console.log(value.value)

  if(!validaFechaDDMMAAAA(value.value))
  {
    var error = document.getElementById('error');
    error.innerHTML = 'Error en la fecha, formato correcto: ej. 10/10/2016';
    var timer = setInterval(function () {
      error.innerHTML = '';
      clearInterval(timer);
    }, 3000);
  } else {
    var array = value.value.split('/');
    diaBuscador = parseInt(array[0]);
    mesDefault = saberMesporNumero(array[1]);
    anoDefault = parseInt(array[2]);
    calendario();
  }
}

function validaFechaDDMMAAAA(fecha){
  var dtCh= "/";
  var minYear=1900;
  var maxYear=2100;
  function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
      var c = s.charAt(i);
      if (((c < "0") || (c > "9"))) return false;
    }
    return true;
  }
  function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){
      var c = s.charAt(i);
      if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
  }
  function daysInFebruary (year){
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
  }
  function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
      this[i] = 31
      if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
      if (i==2) {this[i] = 29}
    }
    return this
  }
  function isDate(dtStr){
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strDay=dtStr.substring(0,pos1)
    var strMonth=dtStr.substring(pos1+1,pos2)
    var strYear=dtStr.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
      if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
      return false
    }
    if (strMonth.length<1 || month<1 || month>12){
      return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
      return false
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
      return false
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
      return false
    }
    return true
  }
  if(isDate(fecha)){
    return true;
  }else{
    return false;
  }
}

function calendario() {
  document.getElementById('año').innerHTML = anoDefault;
  document.getElementById('mes').innerHTML = mesDefault;

  var total = posicion(saberMesporNombre(mesDefault), 1, anoDefault);

  if(total == 0)
    total = 7;

  var start = 1;
  var _calendar = document.getElementById('dias_n');

  var load_n = function () {
    var _calendario = document.getElementById('dias_n');
    _calendario.innerHTML = '';

    var totaldias = parseInt(numeroDias(meses, mesDefault));
    var contMes = 1;

    var totalDiasPasado = calculaDiasMesPasado(saberMesporNombre(mesDefault), total);

    var contMesPasado = totalDiasPasado;

    var numero = '';
    var mod = 1;

    for (var i = 0; i <= 41; i++) {
      var clases = 'dias_n';
      if (contMes > totaldias) {
        contMes = 1;
        mod++;
      }
      if (i % 7 == 0)
      {
        var id = 'dias_' + i;
        _calendario.innerHTML += '<div id="'+id+'" class="row">    </div>';
        _calendario = document.getElementById(id);
      }
      if (i < (total - 1)) {
        clases += ' noActual';
        numero = contMesPasado;
        contMesPasado++;
      }
      else if (i >= (total - 1)) {
        if (mod == 1) {
          clases += ' siActual';
          if(contMes == diaDefault && mesDefault == saberMesporNumero(hoy.getMonth()+1) && anoDefault == hoy.getFullYear()){
            clases += ' active';
            numero = contMes;
          }else {
            numero = contMes;
          }
          if (contMes == diaBuscador) {
            clases += ' pressed';
          }
        } else {
          clases += ' noActual';
          numero = contMes;
        }
        contMes++;
      }
      _calendario.innerHTML += '<p id="'+numero+'" class="'+clases+'" onclick="muestraFecha(this)">'+numero+'</p>';
    }
  };
  load_n();
}