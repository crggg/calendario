
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
  <title>Eventos 2 - Información sobre eventos</title>

  <script type="application/javascript" src="funciones.js"></script>
  <link rel="stylesheet" type="text/css" href="css.css">
</head>

<body>

<div id="info">
  <div>
    <span id="<"><<</span>
    <h5 id="año"></h5>
    <span id=">">>></span>
  </div>
  <div>
    <span id="<mes"><<</span>
    <h5 id="mes"></h5>
    <span id=">mes">>></span>
  </div>
  <div id="dias">

  </div>
    <div id="dias_n">

    </div>
    <div>
        <h5 id="data" class="data">Fecha selecionada: ninguna</h5>
    </div>
    <div>
        <h4>Buscar fecha </h4>
        <form id="buscador" action="#">
            <input type="text" name="fecha" placeholder="Ejemplo: 10/10/2016">
            <input type="button" value="Buscar" onclick="buscarFecha()"/>
        </form>
    </div>
    <div>
        <h5 id="error"></h5>
    </div>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

</body>
</html>